﻿using DetentionRoom.Networking.States.Player;
using UnityEngine;

namespace DetentionRoom.Networking
{
    public class WeaponSoundSpawner : MonoBehaviour
    {
        public AudioSource audioSource;

        public void Play(string weaponType, BoltEntity boltEntity)
        {
            var iPlayer = boltEntity.GetComponent<Player>();
            
            switch (weaponType)
            {
                case "Chalk":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.chalkWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.chalkWeaponState.fireSound.Length)]);
                    break;
                
                case "Stapler":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.staplerWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.staplerWeaponState.fireSound.Length)]);
                    break;
                
                case "Slingshot":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.slingshotWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.slingshotWeaponState.fireSound.Length)]);
                    break;
                
                case "Ruler":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.rulerWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.rulerWeaponState.fireSound.Length)]);
                    break;
                
                case "Sponge":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.spongeWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.spongeWeaponState.fireSound.Length)]);
                    break;
                
                case "Hand":
                    audioSource.PlayOneShot(iPlayer.weaponSystem.handsWeaponState.fireSound[Random.Range(0, iPlayer.weaponSystem.handsWeaponState.fireSound.Length)]);
                    break;
            }
            
            Destroy(gameObject, 3f);
        }
    }
}