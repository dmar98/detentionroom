﻿public class PlayerObject
{
    public readonly BoltConnection BoltConnection;
    public BoltEntity BoltEntity;

    public PlayerObject(BoltConnection bc)
    {
        BoltConnection = bc;
    }
    
    public bool IsServer => BoltConnection == null;
    public bool IsClient => BoltConnection != null;
}
