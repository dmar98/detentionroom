﻿using Bolt;
using DetentionRoom.Networking.States.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Time;

namespace DetentionRoom.Networking
{
    public class SpongeRecharge : MonoBehaviour
    {
        public Slider wetProgress01;

        public TextMeshProUGUI fillText01;

        private float _progress;

        private BoltEntity _player;

        private bool _filled;
        private bool _hasDrySpongeInHand;

        private bool _error;
        
        private void OnTriggerEnter(Collider other)
        {
           ResetProgress();

           if (other.GetComponent<Player>() != null)
           {
               _player = other.GetComponent<Player>().entity;

               var iPlayer = _player.GetState<IPlayer>();

               if (iPlayer.ActiveWeaponIndex == 4 && iPlayer.Weapons[4].IsEquipped && !iPlayer.Weapons[4].IsWet)
               {
                   _hasDrySpongeInHand = true;
               }
           }
        }

        private void OnTriggerStay(Collider other)
        {
            if (_filled)
            {
                wetProgress01.value = 100;
                fillText01.text = "100%";
                _error = true;
            }
            
            if (!_hasDrySpongeInHand)
            {
                _error = true;
            }

            if (_error)
            {
                return;
            }
            
            _progress += deltaTime * 140 / 5;
            
            wetProgress01.value = (int) _progress;

            fillText01.text = (int) _progress + "%";

            if (_progress >= 100)
            {
                var rechargeSponge = RechargeSponge.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
                rechargeSponge.Player = _player;
                rechargeSponge.Send();

                _filled = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            ResetProgress();
        }
        
        private void ResetProgress()
        {
            _hasDrySpongeInHand = false;
            _filled = false;
            _error = false;
            
            _progress = 0;
            fillText01.text = "0%";
            
            wetProgress01.minValue = 0;
            wetProgress01.maxValue = 100;
            wetProgress01.value = 0;
        }
    }
}