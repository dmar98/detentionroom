﻿using System.Collections.Generic;

namespace DetentionRoom.Networking
{
    public static class PlayerObjectRegistry
    {
        private static readonly List<PlayerObject> Players = new List<PlayerObject>();
    
        private static void CreatePlayer(BoltConnection connection)
        {
            var player = new PlayerObject(connection);

            if (player.BoltConnection != null)
            {
                player.BoltConnection.UserData = player;
            }
        
            Players.Add(player);
        }
    
        public static IEnumerable<PlayerObject> AllPlayers => Players;
        public static PlayerObject ServerPlayer
        {
            get { return Players.Find(player => player.IsServer); }
        }
        public static void CreateServerPlayer()
        {
            CreatePlayer(null);
        }
        public static void CreateClientPlayer(BoltConnection connection)
        {
            CreatePlayer(connection);
        }

        public static PlayerObject GetPlayer(BoltConnection connection)
        {
            if (connection == null)
            {
                return ServerPlayer;
            }

            return (PlayerObject) connection.UserData;
        }
    }
}