﻿using Bolt;
using DetentionRoom.Networking.Interfaces;
using DetentionRoom.Scripts;
using DetentionRoom.Scripts.Weapon;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DetentionRoom.Networking.States.Player
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(BoltEntity))]
    [RequireComponent(typeof(AudioListener))]
    public class Player : EntityEventListener<IPlayer>
    {
        #region Variables

        #region Components

        //Collider of the player in form of a capsule.
        private CapsuleCollider _capsuleCollider;

        //Component to listen to sound.
        private AudioListener _audioListener;

        //The camera.
        public Camera playerCamera;

        //The physic component.
        private Rigidbody _rigidBody;

        //The transform component.
        private Transform _playerTransform;

        //The origin position of the ground checking ray
        public Transform collisionTransform;

        //Weapon system
        public WeaponSystem weaponSystem;

        public HUD hud;

        #endregion

        [SerializeField] private float lookSensitivity = 10;
        [SerializeField] private int viewField = 60;
        [SerializeField] private Vector3 cameraRotation = Vector3.zero;

        public bool onGround;
        private bool _jumping;
        private float _jumpingTimer;
        private readonly Vector3 _groundCheck = new Vector3(0, -.05f, 0);

        public Material teacherMaterial;
        public Material studentMaterial;
        public MeshRenderer meshRenderer;

        private Transform _focusedObject;
        private IFocusable _lastFocused;

        private int _activeWeaponIndex;

        public TextMeshProUGUI backUsernameDisplay;
        public TextMeshProUGUI frontUsernameDisplay;


        public AudioClip[] walkSounds;
        private AudioSource _audioSource;
        
        public struct Input
        {
            public Vector2 Movement;
            public Vector2 Mouse;
            public bool IsCrouching;
            public bool IsSprinting;
            public float ScrollWheel;
            public bool Interaction;
            public bool Reload;
            public bool Jump;
        }


        public Input PlayerInput { get; private set; }
        private Vector3 _localEulerAngles;
        private Vector3 _direction;
        private bool _isReloading;
        private int _newCurrentAmmoInMagazine;

        public Transform capsuleTransform;

        
        private static readonly int IsReloading = Animator.StringToHash("IsReloading");
        private static readonly int IsPunching = Animator.StringToHash("IsPunching");

        #endregion

        #region Unity Methods

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (!entity.HasControl)
            {
                return;
            }
            
            //Retrieve input.
            PlayerInput = new Input
            {
                Movement = new Vector2(UnityEngine.Input.GetAxisRaw("Horizontal"),
                    UnityEngine.Input.GetAxisRaw("Vertical")),
                Mouse = new Vector2(UnityEngine.Input.GetAxis("Mouse X"), UnityEngine.Input.GetAxis("Mouse Y")),
                IsCrouching = UnityEngine.Input.GetKey(KeyCode.LeftControl),
                IsSprinting = UnityEngine.Input.GetKey(KeyCode.LeftShift),
                ScrollWheel = UnityEngine.Input.GetAxis("Mouse ScrollWheel"),
                Interaction = UnityEngine.Input.GetKeyDown(KeyCode.E),
                Reload = UnityEngine.Input.GetKeyDown(KeyCode.R),
                Jump = UnityEngine.Input.GetKeyDown(KeyCode.Space)
            };
            
            if (PlayerInput.Reload)
            {
                var currentWeapon = state.Weapons[state.ActiveWeaponIndex];

                if (currentWeapon.CurrentAmmoInMagazine < currentWeapon.MagazineSize &&
                    currentWeapon.EntireAmmo > currentWeapon.CurrentAmmoInMagazine)
                {
                    switch (state.ActiveWeaponIndex)
                    {
                        case 0:
                            StartCoroutine(weaponSystem.chalkWeaponState.Reload());
                            break;
                        
                        case 1:
                            StartCoroutine(weaponSystem.staplerWeaponState.Reload());
                            break;
                        
                        case 2:
                            StartCoroutine(weaponSystem.slingshotWeaponState.Reload());
                            break;
                    }
                }
            }

            if (PlayerInput.Jump && onGround)
            {
                _jumping = true;
                _jumpingTimer = 0.2f;
                //_rigidBody.AddForce(new Vector3(0, jumpForce, 0));
            }

            if (_jumpingTimer > 0)
            {
                _jumpingTimer -= Time.deltaTime;
            }

            if (_jumpingTimer <= 0)
            {
                _jumping = false;
            }


            //Cast a ray
            var ray = playerCamera.ScreenPointToRay(UnityEngine.Input.mousePosition);

            if (Physics.Raycast(ray, out var hit, 3))
            {
                _focusedObject = hit.transform;

                var intractable = _focusedObject.GetComponent<IInteractable>();
                var focusable = _focusedObject.GetComponent<IFocusable>();

                if (intractable != null && PlayerInput.Interaction)
                {
                    intractable.Interact(entity);
                }
                else
                {
                    _lastFocused?.DeFocus();
                }

                if (focusable != null)
                {
                    _lastFocused?.DeFocus();
                    focusable.Focus();
                    _lastFocused = focusable;
                }
            }
            else
            {
                _lastFocused?.DeFocus();
                _lastFocused = null;
            }

            var groundRay = new Ray(collisionTransform.position, _groundCheck);
            onGround = Physics.Raycast(groundRay, out _, 0.25f);
        }

        private void FixedUpdate()
        {
            if (!entity.HasControl)
            {
                return;
            }

            if (Mathf.Abs(PlayerInput.Movement.x) > 0 || Mathf.Abs(PlayerInput.Movement.y) > 0)
            {
                if (!_audioSource.isPlaying)
                {
                    _audioSource.PlayOneShot(walkSounds[Random.Range(0, walkSounds.Length)]);
                }
            }
            
            _direction = _playerTransform.right * PlayerInput.Movement.x +
                         _playerTransform.forward * PlayerInput.Movement.y;
            
            _localEulerAngles = _playerTransform.localEulerAngles +
                                new Vector3(0f, PlayerInput.Mouse.x, 0f) * lookSensitivity;

            _playerTransform.localEulerAngles = _localEulerAngles;

            var velocity = state.CurrentSpeed * _direction.normalized;
            velocity.y = onGround ? 0 : -9.81f;
            if (_jumping)
            {
                velocity.y = 9.81f;
            }

            _rigidBody.velocity = velocity;
            
            DoRotation();
        }

        #endregion

        #region Bolt Callbacks

        //Gets called first - Initialization.
        public override void Attached()
        {
            //Fetch all the references that are required.
            GetRequiredReferences();

            //Disable components that affects game-play due to multiple players in scene.
            DisableComponentsFromOtherPlayers();

            //After fetching the references, initialize proper values.
            InitializeComponents();

            //Set the playerTransform to sync position over the network.
            state.SetTransforms(state.Transform, transform);

            //As owner of this entity (server), setup all the states for this entity.
            if (entity.IsOwner)
            {
                InitializeNetworkStateOfPlayer();
            }

            //Add callbacks to trigger methods when a state-value changes.
            AddCallbacks();
        }

        //When a player gains control over this entity.
        public override void ControlGained()
        {
            hud.SetPlayer(this);
            
            playerCamera.enabled = true;
            _audioListener.enabled = true;
            
            //Init health UI
            hud.RefreshHealth();
            
            hud.RefreshAmmoDisplay();
            
            weaponSystem.chalkWeaponState.skipUpdate = false;
            weaponSystem.staplerWeaponState.skipUpdate = false;
            weaponSystem.slingshotWeaponState.skipUpdate = false;
            weaponSystem.rulerWeaponState.skipUpdate = false;
            weaponSystem.spongeWeaponState.skipUpdate = false;
            weaponSystem.handsWeaponState.skipUpdate = false;
        }

        //Gets called every frame.
        public override void SimulateController()
        {
            //Player Command Input
            var playerCommandInput = PlayerCommand.Create();
            playerCommandInput.Position = _rigidBody.position;
            playerCommandInput.Rotation = _localEulerAngles;
            playerCommandInput.ActiveWeaponIndex = _activeWeaponIndex;
            playerCommandInput.CurrentSpeed = PlayerInput.IsCrouching ? 4 : /*_playerInput.IsSprinting ? 9 :*/ 8;
            playerCommandInput.ColliderHeight = PlayerInput.IsCrouching ? 1 : 2;
            playerCommandInput.IsCrouching = PlayerInput.IsCrouching;
            playerCommandInput.IsSprinting = PlayerInput.IsSprinting;
            entity.QueueInput(playerCommandInput);
        }

        //Execute commands from this controller or/and owner.
        public override void ExecuteCommand(Command command, bool resetState)
        {
            if (!(command is PlayerCommand playerCommand))
            {
                return;
            }
            
            if (entity.IsOwner)
            {
                state.ActiveWeaponIndex = playerCommand.Input.ActiveWeaponIndex;
                state.CurrentSpeed = playerCommand.Input.CurrentSpeed;
                state.ColliderHeight = playerCommand.Input.ColliderHeight;
                state.IsCrouching = playerCommand.Input.IsCrouching;
                state.IsSprinting = playerCommand.Input.IsSprinting;

                _playerTransform.position = playerCommand.Input.Position;
            }

            _capsuleCollider.height = state.IsCrouching ? 1 : 2;
            _capsuleCollider.center = state.IsCrouching ? new Vector3(0, -.5f, 0) : Vector3.zero;

            capsuleTransform.localScale = state.IsCrouching ? new Vector3(1, .5f, 1) : Vector3.one;
            capsuleTransform.localPosition = state.IsCrouching ? new Vector3(0, -.5f, 0) : Vector3.zero;
                
            _playerTransform.localEulerAngles = playerCommand.Input.Rotation;
                
            playerCamera.transform.localPosition = state.IsCrouching ? Vector3.zero : new Vector3(0, .5f, 0);
        }

        #endregion

        #region Custom Methods

        private void WeaponActiveIndexChanged()
        {
            //Disable all weapons.
            foreach (var weapon in weaponSystem.allWeapons)
            {
                weapon.gameObject.SetActive(false);
            }

            //Enable the next one.
            weaponSystem.allWeapons[state.ActiveWeaponIndex].gameObject.SetActive(true);

            //Refresh HUD.
            if (entity.HasControl)
            {
                hud.RefreshWeaponDisplay();
                hud.RefreshAmmoDisplay();
            }
        }

        private void DoRotation()
        {
            cameraRotation = new Vector3(PlayerInput.Mouse.y, 0f, 0f) * lookSensitivity;

            playerCamera.transform.Rotate(-cameraRotation);
            if (playerCamera.transform.localEulerAngles.x > viewField &&
                playerCamera.transform.localEulerAngles.x < 180)
            {
                playerCamera.transform.rotation = Quaternion.identity;
                playerCamera.transform.Rotate(new Vector3(viewField, transform.rotation.eulerAngles.y, 0));
            }
            else if (playerCamera.transform.localEulerAngles.x < 360 - viewField &&
                     playerCamera.transform.localEulerAngles.x > 180)
            {
                playerCamera.transform.rotation = Quaternion.identity;
                playerCamera.transform.Rotate(new Vector3(-viewField, transform.rotation.eulerAngles.y, 0));
            }
        }

        private void TeamChanged()
        {
            meshRenderer.sharedMaterial = state.Team == "Teacher" ? teacherMaterial : studentMaterial;
        }

        private void UsernameChanged()
        {
            backUsernameDisplay.text = state.Username;
            frontUsernameDisplay.text = state.Username;
        }

        private void DisableComponentsFromOtherPlayers()
        {
            playerCamera.enabled = false;
            _audioListener.enabled = false;

            weaponSystem.chalkWeaponState.skipUpdate = true;
            weaponSystem.staplerWeaponState.skipUpdate = true;
            weaponSystem.slingshotWeaponState.skipUpdate = true;
            weaponSystem. rulerWeaponState.skipUpdate = true;
            weaponSystem.spongeWeaponState.skipUpdate = true;
            weaponSystem.handsWeaponState.skipUpdate = true;
        }

        private void GetRequiredReferences()
        {
            _capsuleCollider = GetComponent<CapsuleCollider>();
            _audioListener = GetComponent<AudioListener>();
            playerCamera = GetComponentInChildren<Camera>();
            _rigidBody = GetComponent<Rigidbody>();
            _playerTransform = transform;
            hud = HUD.GetInstance();
            
            weaponSystem.chalkWeaponState.weapon = state.Weapons[0];
            weaponSystem.staplerWeaponState.weapon = state.Weapons[1];
            weaponSystem.slingshotWeaponState.weapon = state.Weapons[2];
            weaponSystem.rulerWeaponState.weapon = state.Weapons[3];
            weaponSystem.spongeWeaponState.weapon = state.Weapons[4];
            weaponSystem.handsWeaponState.weapon = state.Weapons[5];
        }

        private void InitializeComponents()
        {
            //Set the active weapon index to 0.
            _activeWeaponIndex = 0;
            
            _rigidBody.collisionDetectionMode = CollisionDetectionMode.Continuous;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            DisableComponentsFromOtherPlayers();

            playerCamera.enabled = false;
            _audioListener.enabled = false;

            foreach (var go in weaponSystem.allWeapons)
            {
                go.gameObject.SetActive(false);
            }

            weaponSystem.allWeapons[0].gameObject.SetActive(true);
        }

        private void InitializeNetworkStateOfPlayer()
        {
            //Set the start health of the player.
            state.Health = 100;

            //Set the active weapon index to 0.
            state.ActiveWeaponIndex = 0;
            
            state.Weapons[0].Id = 0;
            state.Weapons[0].Label = "Chalk";
            state.Weapons[0].Damage = 5;
            state.Weapons[0].IsEquipped = true;
            state.Weapons[0].EntireAmmo = 20;
            state.Weapons[0].FireRate = 1;
            state.Weapons[0].MaxCarryAmmo = 20;
            state.Weapons[0].ReloadTime = 2;
            state.Weapons[0].SpreadFactor = 0;
            state.Weapons[0].IsReloading = false;
            state.Weapons[0].CurrentAmmoInMagazine = 10;
            state.Weapons[0].MagazineSize = 10;
            state.Weapons[0].MaxDistance = 0;
            state.Weapons[0].Force = 0;
            state.Weapons[0].Player = entity;
            state.Weapons[0].Radius = 0f;
            state.Weapons[0].IsWet = false;


            state.Weapons[1].Id = 1;
            state.Weapons[1].Label = "Stapler";
            state.Weapons[1].Damage = 5;
            state.Weapons[1].IsEquipped = false;
            state.Weapons[1].EntireAmmo = 20;
            state.Weapons[1].FireRate = 10;
            state.Weapons[1].MaxCarryAmmo = 20;
            state.Weapons[1].ReloadTime = 2;
            state.Weapons[1].SpreadFactor = 0;
            state.Weapons[1].IsReloading = false;
            state.Weapons[1].CurrentAmmoInMagazine = 10;
            state.Weapons[1].MagazineSize = 10;
            state.Weapons[1].MaxDistance = 0;
            state.Weapons[1].Force = 0;
            state.Weapons[1].Player = entity;
            state.Weapons[1].Radius = 0f;
            state.Weapons[1].IsWet = false;
            
            
            state.Weapons[2].Id = 2;
            state.Weapons[2].Label = "Slingshot";
            state.Weapons[2].Damage = 5;
            state.Weapons[2].IsEquipped = false;
            state.Weapons[2].EntireAmmo = 20;
            state.Weapons[2].FireRate = 1;
            state.Weapons[2].MaxCarryAmmo = 20;
            state.Weapons[2].ReloadTime = 2;
            state.Weapons[2].SpreadFactor = 0;
            state.Weapons[2].IsReloading = false;
            state.Weapons[2].CurrentAmmoInMagazine = 10;
            state.Weapons[2].MagazineSize = 10;
            state.Weapons[2].MaxDistance = 0;
            state.Weapons[2].Force = 0;
            state.Weapons[2].Player = entity;
            state.Weapons[2].Radius = 0f;
            state.Weapons[2].IsWet = false;


            state.Weapons[3].Id = 3;
            state.Weapons[3].Label = "Ruler";
            state.Weapons[3].Damage = 5;
            state.Weapons[3].IsEquipped = false;
            state.Weapons[3].EntireAmmo = 20;
            state.Weapons[3].FireRate = 1;
            state.Weapons[3].MaxCarryAmmo = 20;
            state.Weapons[3].ReloadTime = 2;
            state.Weapons[3].SpreadFactor = 0;
            state.Weapons[3].IsReloading = false;
            state.Weapons[3].CurrentAmmoInMagazine = 10;
            state.Weapons[3].MagazineSize = 10;
            state.Weapons[3].MaxDistance = 0;
            state.Weapons[3].Force = 0;
            state.Weapons[3].Player = entity;
            state.Weapons[3].Radius = 0f;
            state.Weapons[3].IsWet = false;
            
            
            state.Weapons[4].Id = 4;
            state.Weapons[4].Label = "Sponge";
            state.Weapons[4].Damage = 5;
            state.Weapons[4].IsEquipped = false;
            state.Weapons[4].EntireAmmo = 0;
            state.Weapons[4].FireRate = 1;
            state.Weapons[4].MaxCarryAmmo = 10;
            state.Weapons[4].ReloadTime = 5;
            state.Weapons[4].SpreadFactor = 0;
            state.Weapons[4].IsReloading = false;
            state.Weapons[4].CurrentAmmoInMagazine = 0;
            state.Weapons[4].MagazineSize = 1;
            state.Weapons[4].MaxDistance = 1000;
            state.Weapons[4].Force = 1000;
            state.Weapons[4].Player = entity;
            state.Weapons[4].Radius = 2.5f;
            state.Weapons[4].IsWet = false;


            state.Weapons[5].Id = 5;
            state.Weapons[5].Label = "Hands";
            state.Weapons[5].Damage = 15;
            state.Weapons[5].IsEquipped = true;
            state.Weapons[5].EntireAmmo = 0;
            state.Weapons[5].FireRate = 1;
            state.Weapons[5].MaxCarryAmmo = 0;
            state.Weapons[5].ReloadTime = 3;
            state.Weapons[5].SpreadFactor = 0;
            state.Weapons[5].IsReloading = false;
            state.Weapons[5].CurrentAmmoInMagazine = 0;
            state.Weapons[5].MagazineSize = 0;
            state.Weapons[5].MaxDistance = 1;
            state.Weapons[5].Force = 0;
            state.Weapons[5].Player = entity;
            state.Weapons[5].Radius = 0.5f;
            state.Weapons[5].IsWet = false;
        }

        private void AddCallbacks()
        {
            state.AddCallback("ActiveWeaponIndex", WeaponActiveIndexChanged);
            state.AddCallback("Health", hud.RefreshHealth);
            state.AddCallback("Team", TeamChanged);
            state.AddCallback("Username", UsernameChanged);
            
            state.AddCallback("Weapons[]", UpdateWeapon);
        }

        private void UpdateWeapon(IState iState, string path, ArrayIndices indices)
        {
            var index = indices[0];
            var iPlayer = (IPlayer) iState;
            
            switch (path)
            {
                case "Weapons[].IsReloading":
                    switch (index)
                    {
                        case 0:
                            weaponSystem.chalkWeaponState.animator.SetBool(IsReloading, iPlayer.Weapons[index].IsReloading);
                            break;
                        
                        case 1:
                            weaponSystem.staplerWeaponState.animator.SetBool(IsReloading, iPlayer.Weapons[index].IsReloading);
                            break;
                        
                        case 2:
                            weaponSystem.slingshotWeaponState.animator.SetBool(IsReloading, iPlayer.Weapons[index].IsReloading);
                            break;
                        
                        case 5:
                            weaponSystem.spongeWeaponState.animator.SetBool(IsReloading, iPlayer.Weapons[index].IsReloading);
                            break;
                    }
                    break;
                
                case "Weapons[].CurrentAmmoInMagazine":
                    hud.RefreshAmmoDisplay();
                    break;
                
                case "Weapons[].EntireAmmo":
                    hud.RefreshAmmoDisplay();
                    break;
                
                case "Weapons[].IsPunching":
                    weaponSystem.handsWeaponState.animator.SetBool(IsPunching, iPlayer.Weapons[index].IsPunching);
                    break;
            }
        }
        
        public void ChangeWeaponIndex(int newWeaponIndex)
        {
            _activeWeaponIndex = newWeaponIndex;
        }

        #endregion
        
        #region Bolt Events

        public override void OnEvent(Respawn respawn)
        {
            EventLog.GetInstance().AddEntryLog
            (
                state.Username + " was killed by " + respawn.KilledByPlayerName +" with " + respawn.KilledByWeaponName
            );

            if (respawn.PlayerToRespawn != entity)
            {
                return;
            }

            transform.position = new Vector3(0, 20, 0);
        }
        
        #endregion
    }
}