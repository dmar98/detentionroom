﻿using System.Collections;
using Bolt;
using UnityEngine;

namespace DetentionRoom.Networking.States.Weapons
{
    public class BaseRayCastWeapon : BaseRangeWeapon
    {
        private float _nextTimeToFire;
        
        private void Update()
        {
            if (skipUpdate)
            {
                return;
            }
            
            if (weapon.EntireAmmo == 0 || weapon.IsReloading)
            {
                return;
            }
            
            //Reload weapon
            if (weapon.CurrentAmmoInMagazine <= 0 && weapon.EntireAmmo > 0)
            {
                if (gameObject.activeInHierarchy)
                {
                    StartCoroutine(Reload());
                }
                
                return;
            }

            if (!Input.GetButton("Fire1") || !(Time.time >= _nextTimeToFire) || weapon.EntireAmmo <= 0)
            {
                return;
            }
            
            _nextTimeToFire = Time.time + 1f / weapon.FireRate;
            
            Shoot();
        }
        private void Shoot()
        {
            if (fireSound.Length > 0)
            {
                audioSource.PlayOneShot(fireSound[Random.Range(0, fireSound.Length)]);
            }
            
            var weaponFired = WeaponFired.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            weaponFired.WeaponId = weapon.Id;
            weaponFired.Entity = weapon.Player;
            weaponFired.Send();
            
            var weaponSound = WeaponSound.Create(GlobalTargets.Others, ReliabilityModes.Unreliable);
            weaponSound.Position = transform.position;
            weaponSound.WeaponType = weapon.Label;
            weaponSound.Player = player.entity;
            weaponSound.Send();
            
            var dir = Spread();
            
            if (!Physics.Raycast(cameraTransform.transform.position, dir, out var hit, 100)) return;

            var boltEntity = hit.transform.GetComponent<BoltEntity>();

            if (boltEntity != null && boltEntity.GetState<IPlayer>() != null)
            {
                var playerHit = PlayerHit.Create(GlobalTargets.OnlyServer);
                playerHit.Target = boltEntity;
                playerHit.PlayerWhoShot = player.state.Username;
                playerHit.WeaponName = weapon.Label;
                playerHit.Damage = weapon.Damage;
                playerHit.Send();


                var applyHitIndicator = ApplyHitIndicator.Create(ReliabilityModes.Unreliable);
                applyHitIndicator.Message = weapon.Damage.ToString();
                applyHitIndicator.Position = hit.point;
                applyHitIndicator.Send();
            }
            else
            {
                var ie = ImpactEffect.Create(ReliabilityModes.Unreliable);
                ie.Position = hit.point;
                ie.Send();
            }
        }
        public IEnumerator Reload()
        {
            if (weapon.CurrentAmmoInMagazine == weapon.MagazineSize || weapon.EntireAmmo <= 0 || weapon.IsReloading)
            {
                yield break;
            }
            
            //Send a command to notify that this weapon is reloading.
            var reloadingWeapon = ReloadingWeapon.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            reloadingWeapon.Player = weapon.Player;
            reloadingWeapon.WeaponReloadState = true;
            reloadingWeapon.WeaponId = weapon.Id;
            reloadingWeapon.Send();

            //Wait
            yield return new WaitForSeconds(weapon.ReloadTime - .25f);
            
            //Send a command to notify that this weapon has reloaded.
            reloadingWeapon = ReloadingWeapon.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            reloadingWeapon.Player = weapon.Player;
            reloadingWeapon.WeaponReloadState = false;
            reloadingWeapon.WeaponId = weapon.Id;
            reloadingWeapon.Send();
            
            //Wait
            yield return new WaitForSeconds(.25f);
            
            //Send a command to set the ammo in magazine.
            var weaponReloaded = WeaponReloaded.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            weaponReloaded.Player = weapon.Player;
            weaponReloaded.WeaponId = weapon.Id;
            weaponReloaded.NewCurrentAmount = weapon.EntireAmmo >= weapon.MagazineSize ? weapon.MagazineSize : weapon.EntireAmmo;
            weaponReloaded.Send();
        }
        
        private Vector3 Spread()
        {
            var direction = cameraTransform.forward;
            var sf = weapon.SpreadFactor;
            
            direction.x += Random.Range(-sf, sf);
            direction.y += Random.Range(-sf, sf);
            direction.z += Random.Range(-sf, sf);

            return direction;
        }
    }
}