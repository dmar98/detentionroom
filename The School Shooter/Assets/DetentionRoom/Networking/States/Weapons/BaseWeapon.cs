﻿using UnityEngine;

namespace DetentionRoom.Networking.States.Weapons
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    public abstract class BaseWeapon : MonoBehaviour
    {
        public Weapon weapon;
        public bool skipUpdate;
        public AudioClip[] fireSound;
        public AudioClip reloadSound;
        public bool canSwitch;
        public AudioSource audioSource;
        
        public Animator animator;
        
        public Player.Player player;
        public Transform cameraTransform;

        private void Start()
        {
            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }
    }
}