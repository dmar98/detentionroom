﻿namespace DetentionRoom.Networking.Interfaces
{
    public interface IFocusable
    {
        void Focus();

        void DeFocus();
    }
}