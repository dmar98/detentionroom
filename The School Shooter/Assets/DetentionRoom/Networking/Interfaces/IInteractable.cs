﻿namespace DetentionRoom.Networking
{
    public interface IInteractable
    {
        void Interact(BoltEntity boltEntity);
    }
}