﻿using System;
using System.Collections.Generic;
using Bolt;
using DetentionRoom.Networking.States.Player;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace DetentionRoom.Networking.In_Game
{
    [BoltGlobalBehaviour(BoltNetworkModes.Server)]
    public class NetworkServerInGameCallbacks : GlobalEventListener
    {
        private static void CreatePlayer(IPlayerInfo playerInfo, BoltConnection boltConnection, bool isHost)
        {
            if (isHost)
            {
                PlayerObjectRegistry.CreateServerPlayer();
            }
            else
            {
                PlayerObjectRegistry.CreateClientPlayer(boltConnection);
            }
            
            //If a player has selected no team, assign one randomly
            if (playerInfo.Team == Classes.Unassigned.ToString())
            {
                var random = Random.Range(0, 2);

                playerInfo.Team = random == 1 ? Classes.Teacher.ToString() : Classes.Student.ToString();
            }
            
            //Get all the spawn positions
            var spawnPoints = FindObjectsOfType<SpawnPosition>();
            
            //Initialize value to zero.
            var spawnPosition = Vector3.zero;
            
            //Get all the spawn positions of stage one and of the correct team
            foreach (var spawnPoint in spawnPoints)
            {
                if (spawnPoint.stage == Stage.Dunno && spawnPoint.classType.ToString() == playerInfo.Team)
                {
                    spawnPosition = spawnPoint.transform.position;
                }
            }
            
            //Instantiate the player over the network at the correct position and at 0 rotation
            var networkPlayerEntity = BoltNetwork.Instantiate(BoltPrefabs.Player, spawnPosition, Quaternion.identity);
            
            //Get the player state
            var iPlayer = networkPlayerEntity.GetState<IPlayer>();
            
            //Assign the team and username from the lobby
            iPlayer.Team = playerInfo.Team;
            iPlayer.Username = playerInfo.Username;
            iPlayer.Entity = networkPlayerEntity;
            
            if (isHost)
            {
                networkPlayerEntity.TakeControl();
            }
            else
            {
                networkPlayerEntity.AssignControl(boltConnection);
            }
        }

        public override void OnEvent(PlayerHit playerHit)
        {
            var targetPlayer = playerHit.Target.GetState<IPlayer>();
            targetPlayer.Health -= playerHit.Damage;

            if (targetPlayer.Health <= 0)
            {
                targetPlayer.Health = 100;
                
                var respawn = Respawn.Create(targetPlayer.Entity, EntityTargets.Everyone);
                respawn.PlayerToRespawn = targetPlayer.Entity;
                respawn.KilledByPlayerName = playerHit.PlayerWhoShot;
                respawn.KilledByWeaponName = playerHit.WeaponName;
                respawn.Send();
            }
        }
        
        public override void OnEvent(AmmoPickedUp ammoPickedUp)
        {
            var iPlayer = ammoPickedUp.Player.GetState<IPlayer>();
            var player = iPlayer.Entity.GetComponent<Player>();

            Weapon weapon;

            switch (ammoPickedUp.AmmoType)
            {
                case "Chalk":
                    weapon = player.weaponSystem.chalkWeaponState.weapon;
                    break;
                
                case "Stapler":
                    weapon = player.weaponSystem.staplerWeaponState.weapon;
                    break;
                
                case "Slingshot":
                    weapon = player.weaponSystem.slingshotWeaponState.weapon;
                    break;
                
                case "Ruler":
                    weapon = player.weaponSystem.rulerWeaponState.weapon;
                    break;
                
                case "Sponge":
                    weapon = player.weaponSystem.spongeWeaponState.weapon;
                    break;
                
                case "Hands":
                    weapon = player.weaponSystem.handsWeaponState.weapon;
                    break;
                
                default: throw new ArgumentOutOfRangeException("");
            }
            
            if (!weapon.IsEquipped)
            {
                return;
            }
            
            if (weapon.EntireAmmo + ammoPickedUp.AmmoAmount > weapon.MaxCarryAmmo)
            {
                weapon.EntireAmmo = weapon.MaxCarryAmmo;
            }
            else
            {
                weapon.EntireAmmo += ammoPickedUp.AmmoAmount;
            }
            
            var destroyGameObject = DestroyGameObject.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            destroyGameObject.Entity = ammoPickedUp.ObjectEntity;
            destroyGameObject.Send();
        }

        public override void OnEvent(WeaponPickedUp weaponPickedUp)
        {
            var iPlayer = weaponPickedUp.Player.GetState<IPlayer>();
            
            Weapon weapon;

            switch (weaponPickedUp.WeaponType)
            {
                case "Chalk":
                    weapon = iPlayer.Weapons[0];
                    break;
                
                case "Stapler":
                    weapon = iPlayer.Weapons[1];
                    break;
                
                case "Slingshot":
                    weapon = iPlayer.Weapons[2];
                    break;
                
                case "Ruler":
                    weapon = iPlayer.Weapons[3];
                    break;
                
                case "Sponge":
                    weapon = iPlayer.Weapons[4];
                    break;
                
                case "Hands":
                    weapon = iPlayer.Weapons[5];
                    break;
                
                default: throw new ArgumentOutOfRangeException("");
            }

            if (weapon.IsEquipped)
            {
                return;
            }
            
            weapon.IsEquipped = true;
            weapon.EntireAmmo = weapon.MaxCarryAmmo;
            weapon.CurrentAmmoInMagazine = weapon.MagazineSize;

            iPlayer.ActiveWeaponIndex = weapon.Id;
            
            var destroyGameObject = DestroyGameObject.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            destroyGameObject.Entity = weaponPickedUp.ObjectEntity;
            destroyGameObject.Send();
        }

        public override void OnEvent(RechargeSponge rechargeSponge)
        {
            var iPlayer = rechargeSponge.Player.GetState<IPlayer>();

            iPlayer.Weapons[4].IsWet = true;
        }

        public override void OnEvent(MedicPickedUp medicPickedUp)
        {
            var iPlayer = medicPickedUp.Player.GetState<IPlayer>();

            switch (medicPickedUp.MedicType)
            {
                case "Health":
                    if (iPlayer.Health + medicPickedUp.Amount > 100)
                    {
                        iPlayer.Health = 100;
                    }
                    else
                    {
                        iPlayer.Health += medicPickedUp.Amount;
                    }
                    break;
                
                
                case "Adrenaline":
                    //iPlayer.Adrenaline += supplyPickup.Amount;
                    break;
            }
        }

        public override void OnEvent(SpawnPickup spawnPickup)
        {
            BoltNetwork.Instantiate(BoltPrefabs.Sponge_Weapon_Pickup, spawnPickup.Position, Quaternion.identity);
        }

        public override void OnEvent(WeaponFired weaponFired)
        {
            var iPlayer = weaponFired.Entity.GetState<IPlayer>();
            iPlayer.Weapons[weaponFired.WeaponId].CurrentAmmoInMagazine--;
            iPlayer.Weapons[weaponFired.WeaponId].EntireAmmo--;
            iPlayer.Weapons[weaponFired.WeaponId].IsWet = false;
        }

        public override void OnEvent(PunchFired punchFired)
        {
            var iPlayer = punchFired.Player.GetState<IPlayer>();
            iPlayer.Weapons[punchFired.WeaponId].IsPunching = punchFired.IsPunching;
        }
        
        public override void OnEvent(WeaponReloaded weaponReloaded)
        {
            var iPlayer = weaponReloaded.Player.GetState<IPlayer>();
            iPlayer.Weapons[weaponReloaded.WeaponId].CurrentAmmoInMagazine = weaponReloaded.NewCurrentAmount;
        }

        public override void OnEvent(ReloadingWeapon reloadingWeapon)
        {
            var iPlayer = reloadingWeapon.Player.GetState<IPlayer>();
            iPlayer.Weapons[reloadingWeapon.WeaponId].IsReloading =reloadingWeapon.WeaponReloadState;
        }

        public override void OnEvent(DestroyGameObject destroyGameObject)
        {
            BoltNetwork.Destroy(destroyGameObject.Entity);
        }

        public override void OnEvent(WeaponEquipState weaponEquipState)
        {
            var iPlayer = weaponEquipState.Player.GetState<IPlayer>();
            iPlayer.Weapons[weaponEquipState.WeaponId].IsEquipped = weaponEquipState.State;
        }

        public override void OnEvent(OpenDoor openDoor)
        {
            var door = openDoor.Entity.GetState<IDoor>();

            if (!(door.Cooldown <= 0))
            {
                return;
            }
            
            door.IsOpen = !door.IsOpen;
            door.Cooldown = 3;
        }
        
        public override void SceneLoadLocalDone(string map)
        {
            if(SceneManager.GetActiveScene().name != "PreAlphaMap")
            {
                return;
            }
            
            foreach (var entity in BoltNetwork.Entities)
            {
                if (!entity.StateIs<IPlayerInfo>() || !entity.IsOwner)
                {
                    continue;
                }
                
                var playerInfo = entity.GetState<IPlayerInfo>();
                CreatePlayer(playerInfo, null, true);
                BoltNetwork.Destroy(entity);
                break;
            }
        }

        public override void SceneLoadRemoteDone(BoltConnection connection)
        {
            if(SceneManager.GetActiveScene().name != "PreAlphaMap")
            {
                return;
            }

            foreach (var entity in BoltNetwork.Entities)
            {
                if (!entity.StateIs<IPlayerInfo>() || !entity.IsController(connection))
                {
                    continue;
                }
                
                var playerInfo = entity.GetState<IPlayerInfo>();
                CreatePlayer(playerInfo, connection, false);
                BoltNetwork.Destroy(entity);
                break;
            }
        }
        
        public override void Disconnected(BoltConnection connection)
        {
            foreach (var entity in BoltNetwork.Entities)
            {
                if (!entity.StateIs<IPlayer>() || !entity.IsController(connection))
                {
                    continue;
                }
                
                BoltNetwork.Destroy(entity);
                break;
            }
        }        
    }
}