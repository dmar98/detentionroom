﻿using Bolt;
using DetentionRoom.Networking.Interfaces;
using TMPro;
using UnityEngine;

namespace DetentionRoom.Networking.In_Game
{
    public class Door : EntityEventListener<IDoor>, IInteractable, IFocusable
    {
        public TextMeshProUGUI interactionTextFront;
        public TextMeshProUGUI interactionTextBack;

        public Vector3 openRotation;
        public Vector3 closeRotation;
        public Vector3 openPosition;
        public Vector3 closePosition;

        public override void Attached()
        {
            state.AddCallback("IsOpen", StateChanged);

            if (entity.IsOwner)
            {
                state.Cooldown = 0;
                state.IsOpen = false;
            }
        }
        
        public void Interact(BoltEntity boltEntity)
        {
            var openDoor = OpenDoor.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            openDoor.Entity = entity;
            openDoor.Send();
        }

        public override void SimulateOwner()
        {
            if (state.Cooldown > 0)
            {
                state.Cooldown -= BoltNetwork.FrameDeltaTime;
            }
        }
        
        private void StateChanged()
        {
            transform.localEulerAngles = state.IsOpen ? openRotation : closeRotation;
            transform.localPosition = state.IsOpen ? openPosition : closePosition;
        }

        public void Focus()
        {
            interactionTextFront.text = state.IsOpen ? "Press [e] to close the Door" : "Press [e] to open the Door";
            interactionTextBack.text = state.IsOpen ? "Press [e] to close the Door" : "Press [e] to open the Door";
        }

        public void DeFocus()
        {
            interactionTextFront.text = "";
            interactionTextBack.text = "";
        }
    }
}