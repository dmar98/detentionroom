﻿using TMPro;
using UnityEngine;

namespace DetentionRoom.Networking
{
    public class EventLog : MonoBehaviour
    {
        private static EventLog _instance;

        public TextMeshProUGUI textPrefab;
        public Transform eventLogContainer;

    
        private EventLog() {}
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    
        public static EventLog GetInstance()
        {
            return _instance;
        }

        public void AddEntryLog(string t)
        {
            var eventLog = Instantiate(textPrefab, eventLogContainer);
            eventLog.text = t;

            Destroy(eventLog.gameObject, 3f);
        }
    }
}