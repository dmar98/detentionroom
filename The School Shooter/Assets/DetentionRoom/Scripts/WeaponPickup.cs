﻿using Bolt;
using DetentionRoom.Networking;

namespace DetentionRoom.Scripts
{
    public class WeaponPickup : Pickup<IWeaponPickup>
    {
        public WeaponType weaponType;

        public override void Interact(BoltEntity boltEntity)
        {
            var weaponPickedUp = WeaponPickedUp.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            weaponPickedUp.Player = boltEntity;
            weaponPickedUp.WeaponType = weaponType.ToString();
            weaponPickedUp.ObjectEntity = entity;
            weaponPickedUp.Send();
        }
    }
}