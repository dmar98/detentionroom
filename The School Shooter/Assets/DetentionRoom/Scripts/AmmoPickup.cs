﻿using Bolt;
using DetentionRoom.Networking;

namespace DetentionRoom.Scripts
{
    public class AmmoPickup : Pickup<IAmmoPickup>
    {
        public AmmoType ammoType;
        public int amount;

        public override void Interact(BoltEntity boltEntity)
        {
            var ammoPickedUp = AmmoPickedUp.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            ammoPickedUp.Player = boltEntity;
            ammoPickedUp.AmmoType = ammoType.ToString();
            ammoPickedUp.AmmoAmount = amount;
            ammoPickedUp.ObjectEntity = entity;
            ammoPickedUp.Send();
        }
    }
}