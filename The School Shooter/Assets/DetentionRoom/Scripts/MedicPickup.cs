﻿using System;
using Bolt;
using DetentionRoom.Networking;

namespace DetentionRoom.Scripts
{
    public class MedicPickup : Pickup<IMedicPickup>
    {
        public MedicType medicType;
        public int amount;
        
        public override void Interact(BoltEntity boltEntity)
        {
            var medicPickup = MedicPickedUp.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            medicPickup.Player = boltEntity;
            medicPickup.MedicType = medicType.ToString();
            medicPickup.Amount = amount;
            medicPickup.Send();
            
            BoltNetwork.Destroy(gameObject);
        }
    }
}