﻿using System;
using Bolt;
using DetentionRoom.Networking;
using DetentionRoom.Networking.Interfaces;
using TMPro;
using UnityEngine;

namespace DetentionRoom.Scripts
{
    public abstract class Pickup<T> : EntityBehaviour<T>, IInteractable, IFocusable
    {
        public TextMeshProUGUI interactionTextDisplay;
        private readonly Vector3 _groundCheck = new Vector3(0, -.05f, 0);
        public Transform originOfGroundCheckPosition;
        private bool _onGround;
        private Rigidbody _rigidBody;
        

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            DeFocus();
        }

        public abstract void Interact(BoltEntity boltEntity);
        public void Focus()
        {
            interactionTextDisplay.enabled = true;
        }
        public void DeFocus()
        {
            if (interactionTextDisplay != null)
            {
                interactionTextDisplay.enabled = false;
            }
        }
        private void FixedUpdate()
        {
            var groundRay = new Ray(originOfGroundCheckPosition.position, _groundCheck);
            _onGround = Physics.Raycast(groundRay, out _, 0.25f);

            var velocity = Vector3.zero;
            velocity.y = _onGround ? 0 : -9.81f;

            _rigidBody.velocity = velocity;
        }
    }
}