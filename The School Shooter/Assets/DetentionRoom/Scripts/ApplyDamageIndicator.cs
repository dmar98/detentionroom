﻿using TMPro;
using UnityEngine;

namespace DetentionRoom.Scripts
{
    public class ApplyDamageIndicator : MonoBehaviour
    {
        public TextMeshProUGUI[] textArray;

        public void Display(string message)
        {
            foreach (var text in textArray)
            {
                text.text = message;
            }
            
            Destroy(gameObject, 1f);
        }
    }
}