﻿using UnityEngine;

namespace DetentionRoom.Scripts.Weapon
{
    public abstract class Weapon : MonoBehaviour
    {
        /*//References the network weapon from bolt.
        //protected global::Weapon NetworkWeapon;

        //To which player does this weapon belong.
        public Player player;
        
        //Animator
        public Animator animator;

        public AudioClip reloadSound;
        public AudioClip fireSound;

        private float _nextTimeToFire;
        private static readonly int Reloading = Animator.StringToHash("reloading");
        protected Transform CameraTransform;
        private bool _ready;

        #region Unity Methods

        protected virtual void Start()
        {
            animator = transform.parent.GetComponent<Animator>();
            animator.SetBool(Reloading, false);
        }
        private void Update()
        {
            //Don't execute if custom initialization is not ready, we have no control or while reloading.
            if (!_ready || NetworkWeapon == null || !player.entity.HasControl || NetworkWeapon.IsReloading)
            {
                return;
            }
            
            //Reload weapon
            if (NetworkWeapon.CurrentAmmoInMagazine <= 0 && NetworkWeapon.EntireAmmo > 0)
            {
                if (gameObject.activeInHierarchy)
                {
                    StartCoroutine(Reload());
                }
                
                return;
            }

            if (!Input.GetButton("Fire1") || !(Time.time >= _nextTimeToFire) || NetworkWeapon.EntireAmmo <= 0) return;
            _nextTimeToFire = Time.time + 1f / NetworkWeapon.Firerate;
            Shoot();
        }
        
        #endregion

        public void Initialize(Player playerParameter, Transform cameraTransform)
        {
            _ready = false;
            player = playerParameter;
           // NetworkWeapon = EntityStates.GetWeaponStateByIdFromPlayer(transform.GetSiblingIndex(), player);
            CameraTransform = cameraTransform;
            _ready = true;
        }

        public IEnumerator Reload()
        {
            //Send a command to notify that this weapon is reloading.
            var reloadingWeapon = ReloadingWeapon.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            reloadingWeapon.Player = player.entity;
            reloadingWeapon.WeaponReloadState = true;
            reloadingWeapon.WeaponId = NetworkWeapon.ID;
            reloadingWeapon.Send();

            //Wait
            yield return new WaitForSeconds(NetworkWeapon.ReloadTime - .25f);
            
            //Send a command to notify that this weapon has reloaded.
            reloadingWeapon = ReloadingWeapon.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            reloadingWeapon.Player = NetworkWeapon.Player;
            reloadingWeapon.WeaponReloadState = false;
            reloadingWeapon.WeaponId = NetworkWeapon.ID;
            reloadingWeapon.Send();
            
            //Wait
            yield return new WaitForSeconds(.25f);
            
            //Send a command to set the ammo in magazine.
            var weaponReloaded = WeaponReloaded.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            weaponReloaded.Player = player.entity;
            weaponReloaded.WeaponId = NetworkWeapon.ID;
            weaponReloaded.NewCurrentAmount = NetworkWeapon.EntireAmmo >= NetworkWeapon.MagazineSize ? NetworkWeapon.MagazineSize : NetworkWeapon.EntireAmmo;
            weaponReloaded.Send();
        }

        protected abstract void Shoot();

        protected Vector3 Spread()
        {
            var direction = CameraTransform.forward;
            var spreadFactor = NetworkWeapon.SpreadFactor;
            
            direction.x += Random.Range(-spreadFactor, spreadFactor);
            direction.y += Random.Range(-spreadFactor, spreadFactor);
            direction.z += Random.Range(-spreadFactor, spreadFactor);

            return direction;
        }*/
    }
}