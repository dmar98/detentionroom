﻿namespace DetentionRoom.Scripts.Weapon
{
    public class MultipleUseWeapon : Weapon
    {
        /*protected override void Shoot()
        {
            var weaponFired = WeaponFired.Create(GlobalTargets.OnlyServer, ReliabilityModes.ReliableOrdered);
            weaponFired.WeaponId = NetworkWeapon.ID;
            weaponFired.Entity = player.entity;
            weaponFired.Send();

            var dir = Spread();
            
            if (!Physics.Raycast(CameraTransform.transform.position, dir, out var hit, NetworkWeapon.Range)) return;

            // ReSharper disable once Unity.PreferNonAllocApi
            // RaycastHit[] hits = Physics.RaycastAll(CameraTransform.transform.position, dir, NetworkWeapon.Range, 8, QueryTriggerInteraction.Ignore);

            
            foreach (var h in hits)
            {
                var hitBox = h.transform.GetComponent<CustomHitBox>();
             
                if (hitBox != null)
                {
                    var boltEntity = hitBox.player.entity;
                    var playerHit = PlayerHit.Create(GlobalTargets.OnlyServer);
                    playerHit.Target = boltEntity;
                    playerHit.Source = player.entity;
                    playerHit.WeaponId = _networkWeapon.ID;
                    playerHit.BodyPart = hitBox.bodyPart.ToString();
                    playerHit.Send();

                    break;
                }
            }
            
            
            var boltEntity = hit.transform.GetComponent<BoltEntity>();

            if (boltEntity != null && boltEntity.GetState<IPlayer>() != null)
            {
                var playerHit = PlayerHit.Create(GlobalTargets.OnlyServer);
                playerHit.Target = boltEntity;
                playerHit.Source = player.entity;
                playerHit.WeaponId = NetworkWeapon.ID;
                // playerHit.BodyPart = hitBox.bodyPart.ToString();
                playerHit.Send();
            }
           
            var ie = ImpactEffect.Create(ReliabilityModes.Unreliable);
            ie.Position = hit.point;
            ie.Send();
        }*/
    }
}